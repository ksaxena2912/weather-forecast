package cz.kshitij.weather_forcast.tasks;

public enum ParseResult {OK, JSON_EXCEPTION, CITY_NOT_FOUND}